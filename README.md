# caarry_app

A new Flutter project.

## Features added based on the given Coding Task
- Google and Facebook  Sign In Authentication
- Persist user logged in session after app is closed
- Added layout for phone number authentication and verification using otp
- Added Splash Screen Based on the design file provided
- used BLOC for authentication state management.
- Followed MVVM architecture

## Features/Tasks not completed
- Utilization of GetIt for dependency injection
- No Test Coverage
- Phone number Authentication/Verification is disabled (in the current apk file, the is just UI layout of this feature "NOT ACTUAL PHONE NUMBER AUTHENTICATION")

## Some Things to take note of while going through the flow of the app
- User can go ahead through the mobile number field and OTP Screen by just tapping Next/Submit Button.

## Video/ScreenShot



## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
