import 'package:caarry_app/View/Screens/initial_screen.dart';
import 'package:caarry_app/View/Screens/splash_screen.dart';
import 'package:flutter/material.dart';
import '../View/Screens/home_screen.dart';
import '../View/Screens/onboarding_screen.dart';
import '../View/Screens/otp_screen.dart';
import '../View/Screens/phone_screen.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {

    switch (settings.name) {
      case HomeScreen.routeName:
        return HomeScreen.route();
      case SplashScreen.routeName:
        return SplashScreen.route();
      case IntialScreen.routeName:
        return IntialScreen.route();
      case OnboardingScreen.routeName:
        return OnboardingScreen.route();
      case PhoneScreen.routeName:
        return PhoneScreen.route();
      case MobileOtpScreen.routeName: 
        return MobileOtpScreen.route();
      default:
        return _errorRoute();
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
        builder: (_) => Scaffold(
              appBar: AppBar(
                title: const Text('Error'),
              ),
            ),
        settings: const RouteSettings(name: '/error'));
  }
}
