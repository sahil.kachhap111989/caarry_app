import 'package:caarry_app/View/Screens/phone_screen.dart';
import 'package:caarry_app/ViewModel/Bloc/bloc/auth_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../Widgets/Intro_details.dart';

class OnboardingScreen extends StatefulWidget {
  static const String routeName = '/onboarding';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => const OnboardingScreen(),
        settings: const RouteSettings(name: routeName));
  }

  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  State<OnboardingScreen> createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  void _authenticateWithGoogle(context) {
    BlocProvider.of<AuthBloc>(context).add(GoogleSignInRequested());
  }

  void _authenticateWithFacebook(context) {
    BlocProvider.of<AuthBloc>(context).add(FacebookSignInRequested());
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: BlocConsumer<AuthBloc, AuthState>(
          listener: (context, state) {
            if (state is Authenticated) {
              Navigator.pushNamed(context, '/phoneScreen');
            }

            if (state is AuthError) {
              ScaffoldMessenger.of(context)
                  .showSnackBar(SnackBar(content: Text(state.error!)));
            }
          },
          builder: (context, state) {
            if (state is Authenticating) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            if (state is UnAuthenticated) {
              return Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Align(
                      alignment: Alignment.topRight,
                      child: Padding(
                        padding: const EdgeInsets.only(right: 10.0, top: 12.0),
                        child: SizedBox(
                          child: InkWell(
                            onTap: () {
                              Navigator.pushNamed(context, '/');
                            },
                            child: const Text(
                              'Skip',
                              style: TextStyle(fontSize: 18.0),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const IntroDetails(
                      imagePath: 'onboarding_2.png',
                      introTitle: 'TRAVEL & EARN',
                      introDescription:
                          'Earn extra money while offering some\nspace in your bag or in your vehicle',
                    ),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 25.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ElevatedButton(
                            style: ButtonStyle(
                              fixedSize: MaterialStateProperty.all(Size(
                                  MediaQuery.of(context).size.width / 1.19,
                                  MediaQuery.of(context).size.height / 16.24)),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius:
                                          BorderRadius.circular(10.0))),
                            ),
                            onPressed: () {
                              _authenticateWithFacebook(context);
                            },
                            child: Wrap(
                              children: [
                                Image.asset(
                                  'assets/fb_icon.png',
                                  height: MediaQuery.of(context).size.height /
                                      28.32,
                                  width: MediaQuery.of(context).size.height /
                                      28.32,
                                ),
                                const SizedBox(width: 18.0),
                                const Text(
                                  'Login with Facebook',
                                )
                              ],
                            ),
                          ),
                          const Padding(
                            padding: EdgeInsets.only(left: 15.0, right: 15.0),
                            child: Divider(
                              height: 35,
                              thickness: 1.0,
                            ),
                          ),
                          ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(
                                  Colors.white),
                              foregroundColor: MaterialStateProperty.all<Color>(
                                  Colors.black),
                              fixedSize: MaterialStateProperty.all(Size(
                                  MediaQuery.of(context).size.width / 1.19,
                                  MediaQuery.of(context).size.height / 16.24)),
                              shape: MaterialStateProperty.all<
                                      RoundedRectangleBorder>(
                                  RoundedRectangleBorder(
                                      side: const BorderSide(
                                          color: Color(0xff3E414E)),
                                      borderRadius:
                                          BorderRadius.circular(10.0))),
                            ),
                            onPressed: () {
                              _authenticateWithGoogle(context);
                            },
                            child: Wrap(
                              children: [
                                Image.asset(
                                  'assets/google_icon.png',
                                  height: MediaQuery.of(context).size.height /
                                      28.32,
                                  width: MediaQuery.of(context).size.height /
                                      28.32,
                                ),
                                const SizedBox(width: 22.0),
                                const Text(
                                  'Login with Google',
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  ]);
            }
            return Container();
          },
        ),
      ),
    );
  }
}
