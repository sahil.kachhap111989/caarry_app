import 'package:caarry_app/View/Screens/onboarding_screen.dart';
import 'package:caarry_app/ViewModel/Bloc/bloc/auth_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  static const String routeName = '/';

  static Route route(){
    return MaterialPageRoute(
        builder: (_) => const HomeScreen(),
        settings: const RouteSettings(name: routeName));
  }
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  void _logOut(context) {
    BlocProvider.of<AuthBloc>(context).add(SignOutRequested());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: BlocConsumer<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthError) {
          ScaffoldMessenger.of(context)
              .showSnackBar(SnackBar(content: Text(state.error!)));
        }
        if (state is UnAuthenticated) {
          Navigator.pushNamed(context, '/onboarding');
        }
      },
      builder: (context, state) {
        if (state is Authenticating) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        return Center(
          child: ElevatedButton(
              onPressed: () {
                _logOut(context);
              },
              child: const Text('Logout')),
        );
      },
    ));
  }
}
