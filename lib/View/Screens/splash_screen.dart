import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'onboarding_screen.dart';

class SplashScreen extends StatefulWidget {
 static const String routeName = '/splash';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => const SplashScreen(),
        settings: const RouteSettings(name: routeName));
  }
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    _navigateToOnboarding();
  }
  

  _navigateToOnboarding() async {
    await Future.delayed(const Duration(milliseconds: 3000), () {
       Navigator.pushNamed(context, '/initial');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        color: const Color(0xff362E76),
        child: Center(
          child: Image.asset(
            'assets/splash_logo.png',
            width: MediaQuery.of(context).size.width / 2.56,
            height: MediaQuery.of(context).size.height / 4.56,
          ),
        ),
      ),
    );
  }
}
