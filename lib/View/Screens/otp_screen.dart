import 'package:flutter/material.dart';
import 'package:otp_text_field/otp_field.dart';
import 'package:otp_text_field/style.dart';

class MobileOtpScreen extends StatefulWidget {
  static const String routeName = '/Otp';
  final String? number;

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => const MobileOtpScreen(),
        settings: const RouteSettings(name: routeName));
  }

  const MobileOtpScreen({this.number, Key? key}) : super(key: key);

  @override
  State<MobileOtpScreen> createState() => _MobileOtpScreenState();
}

class _MobileOtpScreenState extends State<MobileOtpScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: const EdgeInsets.all(38.0),
          child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  children: [
                    Image.asset(
                      'assets/mobile_verification.png',
                      height: MediaQuery.of(context).size.height / 2.90,
                      width: MediaQuery.of(context).size.width / 1.07,
                    ),
                    Text(
                      'Verification',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                    Text(
                      'You will get a six digit verification code to',
                      style: Theme.of(context).textTheme.caption,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 60.0, vertical: 0),
                      child: Text('${widget.number}'),
                    ),
                    OTPTextField(
                      length: 6,
                      width: MediaQuery.of(context).size.width,
                      textFieldAlignment: MainAxisAlignment.spaceAround,
                      fieldWidth: 40.0,
                      fieldStyle: FieldStyle.box,
                    )
                  ],
                ),
                Column(
                  children: [
                    Column(children: [
                      Text(
                        'Didn\'t receive a code?',
                        style: Theme.of(context).textTheme.caption,
                      ),
                      InkWell(
                          onTap: () {},
                          child: const Text(
                            'Resend Code',
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color(0xff362E76)),
                          )),
                    ]),
                    const SizedBox(
                      height: 15.0,
                    ),
                    ElevatedButton(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(
                            const Color(0xff362E76)),
                        foregroundColor:
                            MaterialStateProperty.all<Color>(Colors.white),
                        fixedSize: MaterialStateProperty.all(Size(
                            MediaQuery.of(context).size.width / 1.19,
                            MediaQuery.of(context).size.height / 16.24)),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0))),
                      ),
                      onPressed: () {
                        Navigator.popAndPushNamed(context, '/');
                      },
                      child: const Text(
                        'Submit',
                      ),
                    ),
                  ],
                ),
              ]),
        ),
      ),
    );
  }
}
