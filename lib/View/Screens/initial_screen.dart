import 'package:caarry_app/View/Screens/home_screen.dart';
import 'package:caarry_app/View/Screens/onboarding_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class IntialScreen extends StatefulWidget {
  static const String routeName = '/initial';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => const IntialScreen(),
        settings: const RouteSettings(name: routeName));
  }
  const IntialScreen({ Key? key }) : super(key: key);

  @override
  State<IntialScreen> createState() => _IntialScreenState();
}

class _IntialScreenState extends State<IntialScreen> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
      stream: FirebaseAuth.instance.authStateChanges(),
      builder: (context, snapshot){
        if(snapshot.hasData){
          return const HomeScreen();
        }
        return const OnboardingScreen();
      },
    );
  }
}