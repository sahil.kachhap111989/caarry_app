import 'package:caarry_app/View/Screens/otp_screen.dart';
import 'package:flutter/material.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';

class PhoneScreen extends StatefulWidget {
  static const String routeName = '/phoneScreen';

  static Route route() {
    return MaterialPageRoute(
        builder: (_) => const PhoneScreen(),
        settings: const RouteSettings(name: routeName));
  }

  const PhoneScreen({Key? key}) : super(key: key);

  @override
  State<PhoneScreen> createState() => _PhoneScreenState();
}

class _PhoneScreenState extends State<PhoneScreen> {
  final TextEditingController? _phonenumController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: Padding(
          padding: const EdgeInsets.all(38.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                children: [
                  Image.asset(
                    'assets/mobile_otp.png',
                    height: MediaQuery.of(context).size.height / 2.90,
                    width: MediaQuery.of(context).size.width / 1.07,
                  ),
                  Text(
                    'Enter your mobile number',
                    style: Theme.of(context)
                        .textTheme
                        .headline6!
                        .copyWith(color: const Color(0xff362E76)),
                  ),
                  Text(
                    'You will receive a verification code on\nyour phone number',
                    style: Theme.of(context).textTheme.caption,
                  ),
                  const SizedBox(
                    height: 26.0,
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 0, horizontal: 8.0),
                    decoration: BoxDecoration(
                        border:
                            Border.all(color: Colors.black.withOpacity(0.13)),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: InternationalPhoneNumberInput(
                      textFieldController: _phonenumController,
                      onInputChanged: (value) {},
                      selectorConfig: const SelectorConfig(
                          selectorType: PhoneInputSelectorType.DIALOG),
                      cursorColor: Colors.black,
                      formatInput: false,
                      inputDecoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: 'Phone Number',
                          contentPadding:
                              EdgeInsets.only(bottom: 15.0, left: 0),
                          hintStyle:
                              TextStyle(color: Colors.grey, fontSize: 16)),
                    ),
                  )
                ],
              ),
              Column(children: [
                ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(
                        const Color(0xff362E76)),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.white),
                    fixedSize: MaterialStateProperty.all(Size(
                        MediaQuery.of(context).size.width / 1.19,
                        MediaQuery.of(context).size.height / 16.24)),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0))),
                  ),
                  onPressed: () {
                    if (_phonenumController?.text != null) {
                      Navigator.pushReplacement(
                          context,
                          MaterialPageRoute(
                              builder: (context) => MobileOtpScreen(
                                    number: _phonenumController!.text,
                                  )));
                    }
                  },
                  child: const Text(
                    'Next',
                  ),
                ),
              ])
            ],
          ),
        ),
      ),
    );
  }
}
