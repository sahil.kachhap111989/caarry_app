import 'package:flutter/material.dart';

class IntroDetails extends StatelessWidget {
  final String? imagePath;
  final String? introTitle;
  final String? introDescription;

  const IntroDetails({
    Key? key,
    this.imagePath,
    this.introTitle,
    this.introDescription,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Image.asset(
        'assets/$imagePath',
        height: MediaQuery.of(context).size.height / 2.90,
        width: MediaQuery.of(context).size.width / 1.07,
      ),
      Text(
        '$introTitle',
        style: const TextStyle(
            fontSize: 25.0,
            color: Color(0xff362E76),
            fontWeight: FontWeight.bold),
      ),
      Padding(
        padding: EdgeInsets.all(MediaQuery.of(context).size.width / 9.3),
        child: Text(
          '$introDescription',
          style: const TextStyle(color: Color(0xff3E414E)),
        ),
      ),
    ]);
  }
}
