import 'package:caarry_app/Model/Repository/auth_repository.dart';
import 'package:caarry_app/View/Screens/splash_screen.dart';
import 'package:caarry_app/ViewModel/Bloc/bloc/auth_bloc.dart';
import 'package:caarry_app/config.dart/app_router.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return RepositoryProvider(
      create: (context) => AuthRepository(),
      child: BlocProvider(
        create: (context) => AuthBloc(authRepository: RepositoryProvider.of<AuthRepository>(context)),
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'Caarry',
          theme: ThemeData(
              primaryColor: const Color(0xff362E76),
              textTheme:
                  GoogleFonts.poppinsTextTheme(Theme.of(context).textTheme)),
          onGenerateRoute: AppRouter.onGenerateRoute,
          initialRoute: SplashScreen.routeName,
        ),
      ),
    );
  }
}
