part of 'auth_bloc.dart';

@immutable
abstract class AuthEvent extends Equatable{
   @override
  // TODO: implement props
  List<Object?> get props => [];
}

class GoogleSignInRequested extends AuthEvent{}

class FacebookSignInRequested extends AuthEvent{}

class CheckIfUserSignedIn extends AuthEvent{}

class SignOutRequested extends AuthEvent{}
