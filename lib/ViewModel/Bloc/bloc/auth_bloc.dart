import 'package:bloc/bloc.dart';
import 'package:caarry_app/Model/Repository/auth_repository.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepository;
  AuthBloc({required this.authRepository}) : super(UnAuthenticated()) {
    on<GoogleSignInRequested>((event, emit) async {
      emit(Authenticating());
      try {
        await authRepository.signInWithGoogle();
        emit(Authenticated());
      } catch (error) {
        emit(AuthError(error.toString()));
        emit(UnAuthenticated());
      }
    });
    on<SignOutRequested>((event, emit) async {
      emit(Authenticating());
      await authRepository.googleSignOut();
      emit(UnAuthenticated());
    });
    on<FacebookSignInRequested>((event, emit) async {
      emit(Authenticating());
      try {
        await authRepository.signInWithFacebook();
        emit(Authenticated());
      } catch (error) {
        emit(AuthError(error.toString()));
        emit(UnAuthenticated());
      }
    });
  }
}
